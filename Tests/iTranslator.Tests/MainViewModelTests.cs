﻿using iTranslator.Domain;
using iTranslator.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace iTranslator.Tests
{
    [TestClass]
    public class MainViewModelTests
    {
        [TestMethod]
        public void TestMainViewModel()
        {
            var translationsCache = new TranslationsCache(new MockTranslationsProvider());
            var mainViewModel = new MainViewModel(translationsCache);

            mainViewModel.Input = "dog";

            Assert.IsTrue(mainViewModel.SearchWordStatus == SearchWordStatus.MatchFound, "MainViewModel SearchWordStatus incorrect");
            Assert.IsTrue(mainViewModel.Culture == "en-US", "MainViewModel Culture incorrect");
            Assert.IsTrue(mainViewModel.Translations[0].Word == "Hund", "MainViewModel translation incorrect");

            mainViewModel.Input = "rabbit";

            Assert.IsTrue(mainViewModel.SearchWordStatus == SearchWordStatus.NoMatchFound, "MainViewModel SearchWordStatus incorrect");
            Assert.IsTrue(mainViewModel.Culture == string.Empty, "MainViewModel Culture incorrect");
            Assert.IsTrue(mainViewModel.Translations.Length == 0, "MainViewModel translation incorrect");
        }
    }
}