﻿using iTranslator.Domain;
using iTranslator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iTranslator.Tests
{
    public class MockTranslationsProvider : ITranslationsProvider
    {
        public Translations Translations => GenerateMockTranslations();

        private Translations GenerateMockTranslations()
        {
            return new Translations
            {
                Records = new List<Record>
                {
                    new Record
                    {
                        Word = "dog",
                        Culture = "en-US",
                        Links = new List<Link>
                        {
                            new Link
                            {
                                Word = "Hund",
                                Culture = "de-DE"
                            }
                        }
                    },
                    new Record
                    {
                        Word = "cat",
                        Culture = "en-US",
                        Links = new List<Link>
                        {
                            new Link
                            {
                                Word = "Katze",
                                Culture = "de-DE"
                            }
                        }
                    },
                    new Record
                    {
                        Word = "bad",
                        Culture = "en-US",
                        Links = new List<Link>
                        {
                            new Link
                            {
                                Word = "schlecht",
                                Culture = "de-DE"
                            }
                        }
                    },
                    new Record
                    {
                        Word = "übel",
                        Culture = "de-DE",
                        Links = new List<Link>
                        {
                            new Link
                            {
                                Word = "evil",
                                Culture = "en-US"
                            },
                            new Link
                            {
                                Word = "bad",
                                Culture = "en-US"
                            }
                        }
                    }
                }
            };
        }
    }
}
