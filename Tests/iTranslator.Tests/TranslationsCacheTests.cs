﻿using System;
using iTranslator.Domain;
using iTranslator.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace iTranslator.Tests
{
    [TestClass]
    public class TranslationsCacheTests
    {
        [TestMethod]
        public void TestTranslationCacheFindWord()
        {
            var translationsCache = new TranslationsCache(new MockTranslationsProvider());

            var translationResult1 = translationsCache.FindWord("cat");

            Assert.IsTrue(translationResult1.Links[0].Word == "Katze", "TranslationCache FindWord() failed");

            var translationResult2 = translationsCache.FindWord("duck");

            Assert.IsTrue(translationResult2 == null, "TranslationCache FindWord() failed");
        }

        [TestMethod]
        public void TestTranslationCacheFindSimilarWords()
        {
            var translationsCache = new TranslationsCache(new MockTranslationsProvider());

            var translationResult1 = translationsCache.FindSimilarWords("good");

            Assert.IsTrue(translationResult1.Length == 0, "TranslationCache FindSimilarWords() failed");

            var translationResult2 = translationsCache.FindSimilarWords("bad");

            Assert.IsTrue(translationResult2[0] == "evil", "TranslationCache FindSimilarWords() failed");
        }
    }
}
