﻿using System;

using Android.App;
using Android.Content.PM;
using Android.OS;
using Lottie.Forms.Droid;

namespace iTranslator.Droid
{
    [Activity(Label = "iTranslator", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static MainActivity Instance { get; private set; }

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            global::Xamarin.Forms.Forms.Init(this, bundle);

            AnimationViewRenderer.Init();
            Instance = this;
            LoadApplication(new App());
        }
    }
}

