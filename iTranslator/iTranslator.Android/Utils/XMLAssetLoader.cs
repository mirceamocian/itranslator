﻿using System.IO;
using iTranslator.Droid.Utils;
using iTranslator.Utils;
using Xamarin.Forms;

[assembly: Dependency(typeof(XMLAssetLoader))]

namespace iTranslator.Droid.Utils
{
    public class XMLAssetLoader : IXMLAssetLoader
    {
        public StreamReader GetXMLDocument(string fileName)
        {
            var assets = MainActivity.Instance.Assets;
            var sr = new StreamReader(assets.Open(fileName));
            return sr;
        }
    }
}