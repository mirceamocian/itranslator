﻿using System;
using GalaSoft.MvvmLight.Ioc;
using iTranslator.Domain;
using iTranslator.ViewModels;
using iTranslator.Views;
using Xamarin.Forms;

namespace iTranslator
{
    public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();
            RegisterDependencies();
            MainPage = new MainPage();
		}

        private void RegisterDependencies()
        {
            SimpleIoc.Default.Register<ITranslationsProvider, TranslationsProvider>();
            SimpleIoc.Default.Register<ITranslationsCache, TranslationsCache>();
            new ViewModelLocator();
        }
    }
}
