﻿using iTranslator.Models;
using System.Collections.Generic;

namespace iTranslator.Domain
{
    public interface ITranslationsCache
    {
        Record FindWord(string word);
        string[] FindSimilarWords(string word);
    }
}
