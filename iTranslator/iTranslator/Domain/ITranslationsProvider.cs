﻿using iTranslator.Models;

namespace iTranslator.Domain
{
    public interface ITranslationsProvider
    {
        Translations Translations { get; }
    }
}
