﻿namespace iTranslator.Domain
{
    public enum SearchWordStatus
    {
        Empty,
        MatchFound,
        NoMatchFound
    }
}
