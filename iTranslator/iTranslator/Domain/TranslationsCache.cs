﻿using iTranslator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTranslator.Domain
{
    public class TranslationsCache : ITranslationsCache
    {
        public readonly ITranslationsProvider _translationsProvider;
        public Dictionary<string, List<Record>> SimilarWords;

        public TranslationsCache(ITranslationsProvider translationsProvider)
        {
            _translationsProvider = translationsProvider;
            GenerateWordDictionary();
        }

        private void GenerateWordDictionary()
        {
            SimilarWords = new Dictionary<string, List<Record>>();
            foreach (var record in _translationsProvider.Translations.Records)
            {
                foreach (var link in record.Links)
                {
                    if (SimilarWords.ContainsKey(link.Word))
                    {
                        SimilarWords[link.Word].Add(record);
                    }
                    else
                    {
                        SimilarWords.Add(link.Word.ToLower(), new List<Record> { record });
                    }
                }
            }
        }

        public string[] FindSimilarWords(string word)
        {
            if (!SimilarWords.ContainsKey(word))
            {
                return new string[0];
            }

            var similarWords = new List<string>();
            foreach (var record in SimilarWords[word.ToLower()])
            {
                foreach (var link in record.Links)
                {
                    if (!similarWords.Contains(link.Word) && link.Word.ToLower() != word)
                    {
                        similarWords.Add(link.Word);
                    }
                }
            }

            return similarWords.ToArray();
        }

        public Record FindWord(string word)
        {
            return _translationsProvider.Translations.Records.FirstOrDefault(record => record.Word.ToLower() == word.ToLower());
        }
    }
}
