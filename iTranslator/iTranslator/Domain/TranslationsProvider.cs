﻿using iTranslator.Models;
using iTranslator.Utils;
using System.Xml.Serialization;
using Xamarin.Forms;

namespace iTranslator.Domain
{
    public class TranslationsProvider : ITranslationsProvider
    {
        public Translations Translations => LoadTranslations();

        private Translations LoadTranslations()
        {
            var serializer = new XmlSerializer(typeof(Translations));
            var xml = DependencyService.Get<IXMLAssetLoader>().GetXMLDocument("Intelligent_translator.xml");
            var translations = (Translations) serializer.Deserialize(xml);
            return translations;
        }
    }
}
