﻿using System.Xml.Serialization;

namespace iTranslator.Models
{
    [XmlRoot(ElementName="LINK")]
    public class Link
    {
        [XmlAttribute(AttributeName = "word")]
        public string Word { get; set; }
        [XmlAttribute(AttributeName = "culture")]
        public string Culture { get; set; }
    }
}
