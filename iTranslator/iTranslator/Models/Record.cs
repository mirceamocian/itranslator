﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace iTranslator.Models
{
    [XmlRoot(ElementName = "RECORD")]
    public class Record
    {
        [XmlAttribute(AttributeName = "word")]
        public string Word { get; set; }
        [XmlAttribute(AttributeName = "culture")]
        public string Culture { get; set; }
        [XmlElement(ElementName = "LINK")]
        public List<Link> Links { get; set; }
    }
}
