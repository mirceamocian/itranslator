﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace iTranslator.Models
{
    [XmlRoot(ElementName = "TRANSLATIONS")]
    public class Translations
    {
        [XmlElement(ElementName = "RECORD")]
        public List<Record> Records { get; set; }
    }
}
