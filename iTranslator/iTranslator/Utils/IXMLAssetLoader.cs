﻿using System.IO;

namespace iTranslator.Utils
{
    public interface IXMLAssetLoader
    {
        StreamReader GetXMLDocument(string fileName);
    }
}
