﻿using System;
using GalaSoft.MvvmLight;
using iTranslator.Domain;
using iTranslator.Models;

namespace iTranslator.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private readonly ITranslationsCache _translationsCache;

        public MainViewModel(ITranslationsCache translationsCache)
        {
            _translationsCache = translationsCache;
            SearchWordStatus = SearchWordStatus.Empty;
        }

        private string _input;
        public string Input
        {
            get => _input;
            set
            {
                Set(ref _input, value);
                TranslateWords();
            }
        }

        private void TranslateWords()
        {
            if (string.IsNullOrEmpty(Input))
            {
                SimilarWords = new string[0];
                Translations = new Link[0];
                SearchWordStatus = SearchWordStatus.Empty;
                Culture = string.Empty;
            }
            else
            {
                var record = _translationsCache.FindWord(Input);
                if (record == null)
                {
                    SimilarWords = new string[0];
                    Translations = new Link[0];
                    SearchWordStatus = SearchWordStatus.NoMatchFound;
                    Culture = string.Empty;
                }
                else
                {
                    SimilarWords = _translationsCache.FindSimilarWords(Input);
                    Translations = record.Links.ToArray();
                    SearchWordStatus = SearchWordStatus.MatchFound;
                    Culture = record.Culture;
                }
            }
        }

        private SearchWordStatus _searchWordStatus;
        public SearchWordStatus SearchWordStatus
        {
            get => _searchWordStatus;
            set => Set(ref _searchWordStatus, value);
        }

        private Link[] _translations;
        public Link[] Translations
        {
            get => _translations;
            set => Set(ref _translations, value);
        }

        private string[] _similarWords;
        public string[] SimilarWords
        {
            get => _similarWords;
            set => Set(ref _similarWords, value);
        }

        private string _culture;
        public string Culture
        {
            get => _culture;
            set => Set(ref _culture, value);
        }
    }
}
