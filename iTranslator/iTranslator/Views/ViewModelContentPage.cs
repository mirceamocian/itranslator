﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Xamarin.Forms;

namespace iTranslator.Views
{
    public class ViewModelContentPage<TViewModel> : ContentPage
        where TViewModel : ViewModelBase
    {
        public ViewModelContentPage(TViewModel viewModel = null)
        {
            BindingContext = viewModel ?? SimpleIoc.Default.GetInstance<TViewModel>();
        }
    }
}
